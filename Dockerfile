FROM registry.centos.org/centos/centos:7

MAINTAINER Karanbir Singh <container-feedback.inb@karan.org>

ENV ZABBIX_VER=3.0.7-1.el7

LABEL License=GPLv2
LABEL Version=${GOGS_VER}

ADD cbs-el7-infra.repo /etc/yum.repos.d/cbs-el7-infra.repo

RUN yum -y --setopt=tsflags=nodocs \
        --enablerepo=cbs-el7-infra-testing \
        install zabbix-agent-${ZABBIX_VER} && \
    yum -y upgrade && yum clean all

RUN chmod 777 /run /var/run /var/log/zabbix

USER 1001

ENTRYPOINT ["/usr/sbin/zabbix_agentd"]